package com.demo.demotablayout.models

class Item(
    val title: String,
    val date: String
)