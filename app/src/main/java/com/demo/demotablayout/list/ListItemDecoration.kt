package com.demo.demotablayout.list

import android.graphics.Canvas
import android.graphics.Rect
import android.graphics.drawable.Drawable
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.demo.demotablayout.convertToPosition

class ListItemDecoration(
    private val leftPending: Int,
    private val divider: Drawable
) : RecyclerView.ItemDecoration() {


    override fun getItemOffsets(
        outRect: Rect,
        view: View,
        parent: RecyclerView,
        state: RecyclerView.State
    ) {
        outRect.set(0, 0, 0, divider.intrinsicHeight)
    }

    override fun onDraw(c: Canvas, parent: RecyclerView, state: RecyclerView.State) {
        super.onDraw(c, parent, state)
        setDividerBounds(c, parent)
    }

    private fun setDividerBounds(canvas: Canvas, parent: RecyclerView) {
        val left  = parent.paddingLeft + parent.context.convertToPosition(leftPending)
        val right = parent.width - parent.paddingRight
        val childCount = parent.childCount

        for (i in 0 until childCount) {
            val child  = parent.getChildAt(i)
            val params = child.layoutParams as RecyclerView.LayoutParams
            val top    = child.bottom + params.bottomMargin
            val bottom = top + divider.intrinsicHeight

            val position = parent.getChildAdapterPosition(child)
            val totalCount = parent.adapter?.itemCount?: 0
            if (position != totalCount - 1)
                divider.setBounds(left, top, right, bottom)
            else
                divider.setBounds(parent.paddingLeft, top, right, bottom)

            divider.draw(canvas)
        }
    }
}