package com.demo.demotablayout.list

import com.demo.demotablayout.dateFormat
import com.demo.demotablayout.models.Item
import java.util.*

object ListData {

    private const val Date_Format = "MMMM dd yyyy, EEEE"

    fun getData(): List<Item> {
        val calendars = arrayListOf<Item>()
        val calendar = Calendar.getInstance()
        calendars.add(getItem(calendar, 0))
        for(i in 1..7) {
            calendar.add(Calendar.DAY_OF_MONTH, -1)
            calendars.add(getItem(calendar, i))
        }
        return calendars
    }

    private fun getItem(calendar: Calendar, i: Int): Item =
        Item("Item $i", calendar.dateFormat(Date_Format))
}