package com.demo.demotablayout

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.viewpager.widget.ViewPager
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), DemoPagerAdapter.Listener {

    private val pageList = arrayListOf<Int>()
    private val viewAdapter: DemoPagerAdapter by lazy { DemoPagerAdapter( pageList, this) }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        pageList.add(R.layout.fragment_tab_one)
        pageList.add(R.layout.fragment_list)

        viewPager.apply {
            adapter = viewAdapter
            currentItem = 0
        }

        tabLayout.setupWithViewPager(viewPager)
    }

    override fun onPageSelected(position: Int) {
        viewPager.currentItem = position
    }
}
