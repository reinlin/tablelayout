package com.demo.demotablayout.adapter

import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.demo.demotablayout.R
import com.demo.demotablayout.models.Item

class ItemViewHolder(view: View): RecyclerView.ViewHolder(view) {

    private val titleTextView: TextView = view.findViewById(R.id.titleTextView)
    private val dateTextView: TextView  = view.findViewById(R.id.dateTextView)

    fun bind(item: Item) {
        titleTextView.text = item.title
        dateTextView.text  = item.date
        println("bind: $item")
    }
}