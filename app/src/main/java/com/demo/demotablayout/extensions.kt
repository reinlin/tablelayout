package com.demo.demotablayout

import android.content.Context
import java.text.SimpleDateFormat
import java.util.*

fun Context.convertToPosition(dp: Int): Int {
    return (dp * resources.displayMetrics.density).toInt()
}

fun Calendar.dateFormat(format: String): String {
    val df = SimpleDateFormat(format, Locale.TAIWAN);
    return df.format(this.time)
}