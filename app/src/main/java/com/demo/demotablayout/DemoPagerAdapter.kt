package com.demo.demotablayout

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager.widget.PagerAdapter
import com.demo.demotablayout.adapter.ListAdapter
import com.demo.demotablayout.list.ListItemDecoration
import com.demo.demotablayout.list.ListData

class DemoPagerAdapter(
    private val pageList: List<Int>,
    private val listener: Listener?
) : PagerAdapter() {

    interface Listener {
        fun onPageSelected(position: Int)
    }

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view == `object`
    }

    override fun getCount(): Int {
        return pageList.size
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return when (position) {
            0 -> "Tab1"
            1 -> "Tab2"
            else -> ""
        }
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val view = LayoutInflater.from(container.context).inflate(pageList[position], container, false)
        println("instantiateItem($position)")
        when (position) {
            0 -> setTabOneView(view)
            1 -> setListView(view, container.context)
        }
        container.addView(view)
        return view
    }

    private fun setTabOneView(view: View) {
        val button = view.findViewById<Button>(R.id.switchTabButton)
        button.setOnClickListener { listener?.onPageSelected(1) }
    }

    private fun setListView(view: View, context: Context) {
        val listView = view.findViewById<RecyclerView>(R.id.listRecyclerView)
        listView.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = ListAdapter().apply { this.setData(ListData.getData()) }
            addItemDecoration(
                ListItemDecoration(
                    20,
                    resources.getDrawable(R.drawable.divider, null)
                )
            )
        }
    }
}